//
//  SAVNavigation.m
//  Maquma
//
//  Created by Aleksandr Sadikov on 12.08.13.
//  Copyright © 2013 Aleksandr Sadikov. All rights reserved.
//

#import "SAVNavigation.h"

#define kSectionIndicatorLineWidth 280.0
#define kSectionIndicatorPadding (44.0 + 28.0)

@interface SAVNavigation () <UIScrollViewDelegate> {
    
    UIView *sectionIndicator;
    UIView *sectionPlace;
}

@property UIScrollView *navigationScroll;
@property int currentIndex;

@end

@implementation SAVNavigation

#pragma mark - Synthesize

@synthesize navigationScroll;
@synthesize viewControllers;

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.indicatorType = IndicatorLine;
    self.indicatorPosition = IndicatorPositionBottom;
}

#pragma mark - Getters & Setters

- (NSArray *)viewControllers {
    
    return viewControllers;
}

- (void)setViewControllers:(NSArray *)controllers {
    
    viewControllers = controllers;
    
    navigationScroll = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    navigationScroll.contentSize = CGSizeMake(navigationScroll.frame.size.width * viewControllers.count, navigationScroll.frame.size.height);
    navigationScroll.contentOffset = CGPointMake(self.currentIndex * navigationScroll.frame.size.width, 0);
    navigationScroll.delegate = self;
    navigationScroll.bounces = NO;
    navigationScroll.pagingEnabled = YES;
    navigationScroll.showsHorizontalScrollIndicator = NO;
    navigationScroll.showsVerticalScrollIndicator = NO;
    
    [viewControllers enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        UIViewController *viewController = (UIViewController *)obj;
        
        CGRect frame = viewController.view.frame;
        frame.origin.x = (self.view.bounds.size.width * idx);
        viewController.view.frame = frame;
        
        [navigationScroll addSubview:viewController.view];
    }];
    
    [self.view addSubview:navigationScroll];
    
    [self initSectionIndicator];
}

#pragma mark - Initialization helper methods

- (CGFloat)indicatorPositionY {
    
    switch (self.indicatorPosition) {
            
        case IndicatorPositionTop:
            
            return kSectionIndicatorPadding;
            
        case IndicatorPositionCenter:
            
            return self.view.frame.size.height / 2;
            
        case IndicatorPositionBottom:
            
            return self.view.frame.size.height - kSectionIndicatorPadding;
    }
}

- (CGFloat)placePositionY {
    
    switch (self.indicatorPosition) {
            
        case IndicatorPositionTop:

            return -0.5;
            
        case IndicatorPositionCenter:
            
            return -1.5;
            
        case IndicatorPositionBottom:
            
            return -2;
    }
}

- (void)initSectionIndicator {
    
    if (self.indicatorType == IndicatorLine) {
        
        sectionIndicator = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 2 - kSectionIndicatorLineWidth / 2, self.indicatorPositionY, kSectionIndicatorLineWidth, 1)];
        sectionIndicator.backgroundColor = [UIColor colorWithWhite:1 alpha:.5];
        
        sectionPlace = [[UIView alloc] initWithFrame:CGRectMake((kSectionIndicatorLineWidth / viewControllers.count) * (navigationScroll.contentOffset.x / navigationScroll.bounds.size.width), self.placePositionY, kSectionIndicatorLineWidth / viewControllers.count, 3)];
        sectionPlace.backgroundColor = [UIColor colorWithWhite:1 alpha:.9];
        
        [sectionIndicator addSubview:sectionPlace];

    } else {
        
        sectionIndicator = [[DotIndicatorView alloc] initWithCount:viewControllers.count];
        CGRect frame = sectionIndicator.frame;
        frame.origin = CGPointMake(self.view.frame.size.width / 2 - (viewControllers.count * 12.0 + viewControllers.count * 6.0) / 2, self.indicatorPositionY);
        [sectionIndicator setFrame:frame];
    }

    sectionIndicator.userInteractionEnabled = NO;
    sectionIndicator.alpha = 0;
    
    [self.view addSubview:sectionIndicator];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (self.indicatorType == IndicatorLine) {
        
        float offset = (kSectionIndicatorLineWidth / viewControllers.count) * (scrollView.contentOffset.x / navigationScroll.bounds.size.width);
        float width = (offset > 185) ? kSectionIndicatorLineWidth / viewControllers.count + 1 : kSectionIndicatorLineWidth / viewControllers.count;
        sectionPlace.frame = CGRectMake(offset, self.placePositionY, width, 3);
        
    } else {
        
        int position = (int)(scrollView.contentOffset.x / navigationScroll.bounds.size.width);

        DotIndicatorView *view = (DotIndicatorView *)sectionIndicator;
        [view setSelectionIndex:position];
    }
    
    if (sectionIndicator.alpha != .8) {
        
        [UIView animateWithDuration:.45 delay:0 options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseIn animations:^{
            
            sectionIndicator.alpha = .8;
            
        } completion:nil];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    if (!decelerate) {
        
        [self scrollViewDidEndDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    [UIView animateWithDuration:.45 delay:.8 options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseOut animations:^{
        
        sectionIndicator.alpha = 0;
        
    } completion:nil];
}

@end

@implementation DotIndicatorView {
    
    NSMutableArray *dots;
}

@synthesize selectionIndex = _selectionIndex;

- (instancetype)initWithCount:(NSInteger)count {
    
    CGFloat width = count * 12.0 + count * 6.0;
    
    self = [super initWithFrame:CGRectMake(0, 0, width, 12.0)];
    
    if (self) {
        
        dots = [[NSMutableArray alloc] init];
        
        CGFloat x = 0;
        for (int i = 0; i < count; i++) {
            
            DotView *dotView = [[DotView alloc] initWithFrame:CGRectMake(x, 0, 12, 12)];
            [self addSubview:dotView];
            x += 18.0;
            
            [dots addObject:dotView];
        }
    }
    
    return self;
}

- (NSInteger)selectionIndex {
    
    return _selectionIndex;
}

- (void)setSelectionIndex:(NSInteger)index {
    
    _selectionIndex = index;
    
    for (int i = 0 ; i < dots.count; i++) {
        
        DotView *dotView = (DotView *)[dots objectAtIndex:i];
        
        [dotView setSelected:(i == index)];
    }
}

@end

@implementation DotView

@synthesize selected = _selected;

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self setBackgroundColor:UIColor.clearColor];
    }
    
    return self;
}

- (BOOL)isSelected {
    
    return _selected;
}

- (void)setSelected:(BOOL)selected {
    
    _selected = selected;
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    
    UIBezierPath* ovalPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.bounds.size.width / 2];
    
    if (self.selected) {
        
        [UIColor.whiteColor setFill];
        [ovalPath fill];
        
    } else {
        
        [UIColor.whiteColor setStroke];
        ovalPath.lineWidth = 0.8;
        [ovalPath stroke];
    }
}

@end
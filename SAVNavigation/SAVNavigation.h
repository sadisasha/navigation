//
//  SAVNavigation.h
//  Maquma
//
//  Created by Aleksandr Sadikov on 12.08.13.
//  Copyright © 2013 Aleksandr Sadikov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, IndicatorType) {
    
    IndicatorLine,
    IndicatorDot
};

typedef NS_ENUM(NSUInteger, IndicatorPosition) {

    IndicatorPositionTop,
    IndicatorPositionCenter,
    IndicatorPositionBottom
};

@interface SAVNavigation : UIViewController

@property (nonatomic, strong) NSArray *viewControllers;

@property (nonatomic) IndicatorType indicatorType;
@property (nonatomic) IndicatorPosition indicatorPosition;

@end

@interface DotIndicatorView : UIView

- (instancetype)initWithCount:(NSInteger)count;

@property (nonatomic) NSInteger selectionIndex;

@end


@interface DotView : UIView

@property (nonatomic, getter=isSelected, setter=setSelected:) BOOL selected;

@end
//
//  ThreeViewController.m
//  NavigationExample
//
//  Created by Aleksandr Sadikov on 02.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import "ThreeViewController.h"

@interface ThreeViewController ()

@property (strong, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation ThreeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.contentLabel setText:self.contentText];
}

@end

//
//  ViewController.m
//  NavigationExample
//
//  Created by Aleksandr Sadikov on 02.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import "LineIndicatorViewController.h"

#import "OneViewController.h"
#import "SecondViewController.h"
#import "ThreeViewController.h"

@interface LineIndicatorViewController ()

@property (nonatomic, strong) OneViewController *oneViewController;
@property (nonatomic, strong) SecondViewController *secondViewController;
@property (nonatomic, strong) ThreeViewController *threeViewController;

@end

@implementation LineIndicatorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.oneViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OneController"];
    self.oneViewController.contentText = @"One";
    
    self.secondViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SecondController"];
    self.secondViewController.contentText = @"Second";
    
    self.threeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ThreeController"];
    self.threeViewController.contentText = @"Three";
    
    self.viewControllers = [NSArray arrayWithObjects:self.oneViewController, self.secondViewController, self.threeViewController, nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  ThreeViewController.h
//  NavigationExample
//
//  Created by Aleksandr Sadikov on 02.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThreeViewController : UIViewController

@property (strong, nonatomic) NSString *contentText;

@end

//
//  OneViewController.m
//  NavigationExample
//
//  Created by Aleksandr Sadikov on 02.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import "OneViewController.h"

@interface OneViewController ()

@property (strong, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation OneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.contentLabel setText:self.contentText];
}

@end

//
//  SecondViewController.m
//  NavigationExample
//
//  Created by Aleksandr Sadikov on 02.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@property (strong, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.contentLabel setText:self.contentText];
}

@end

//
//  DotIndicatorViewController.h
//  NavigationExample
//
//  Created by Aleksandr Sadikov on 03.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SAVNavigation.h"

@interface DotIndicatorViewController : SAVNavigation

@end

//
//  MainViewController.m
//  NavigationExample
//
//  Created by Aleksandr Sadikov on 03.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import "MainViewController.h"

#import "LineIndicatorViewController.h"
#import "DotIndicatorViewController.h"

@interface MainViewController ()

@property (strong, nonatomic) IBOutlet UISegmentedControl *positionSegmentedControl;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.positionSegmentedControl setSelectedSegmentIndex:self.positionSegmentedControl.numberOfSegments - 1];
}

- (IndicatorPosition)indicatorPosition {
    
    switch (self.positionSegmentedControl.selectedSegmentIndex) {
            
        case 0:
            
            return IndicatorPositionTop;
            
        case 1:
            
            return IndicatorPositionCenter;
            
        default:
            
            return IndicatorPositionBottom;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    UIViewController *controller = [segue destinationViewController];
    
    if ([[segue identifier] isEqual: @"line"]) {
        
        LineIndicatorViewController *lineIndicatorViewController = (LineIndicatorViewController *)controller;
        
        lineIndicatorViewController.indicatorPosition = [self indicatorPosition];
        lineIndicatorViewController.indicatorType = IndicatorLine;
        
    } else {
        
        DotIndicatorViewController *dotIndicatorViewController = (DotIndicatorViewController *)controller;
        
        dotIndicatorViewController.indicatorPosition = [self indicatorPosition];
        dotIndicatorViewController.indicatorType = IndicatorDot;
    }
}

@end
